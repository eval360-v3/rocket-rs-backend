#[macro_use]
extern crate rocket;

use rocket::{Build, Rocket};
use crate::repository::init_db;

pub mod web;
pub mod model;
pub mod utility;
pub mod repository;

use std::alloc::System;

#[launch]
fn rocket() -> Rocket<Build> {
    // web::init().attach(init_db())
    web::init()
}
