use rocket::time::{format_description, Date, Month, OffsetDateTime};

pub const DATE_PATTERN: &'static str = "[year]-[month]-[day]";
pub const DATE_TIME_PATTERN: &'static str = "[year]-[month]-[day] [hour]:[minute]:[second]";

pub fn get_date_time_vn_now() -> OffsetDateTime {
    let utc_now = OffsetDateTime::now_utc();
    let hour_vn_now = utc_now.hour() + 7;

    //TODO: Sai
    utc_now.replace_hour(hour_vn_now).unwrap()
}

pub fn get_date_vn_now() -> Date {
    get_date_time_vn_now().date()
}

pub fn get_year_vn_now() -> i32 {
    get_date_time_vn_now().year()
}

pub fn to_date_str(date: Date) -> String {
    let format = format_description::parse(DATE_PATTERN).unwrap();
    date.format(&format).unwrap()
}

pub fn to_date_time_str(date: OffsetDateTime) -> String {
    let format = format_description::parse(DATE_TIME_PATTERN).unwrap();
    date.format(&format).unwrap()
}

pub fn get_number(month: &Month) -> u8 {
    match month {
        Month::January => 1,
        Month::February => 2,
        Month::March => 3,
        Month::April => 4,
        Month::May => 5,
        Month::June => 6,
        Month::July => 7,
        Month::August => 8,
        Month::September => 9,
        Month::October => 10,
        Month::November => 11,
        Month::December => 12,
    }
}
