use math::round;

pub fn div(x: u32, y: u32) -> f64 {
    if x == 0 || y == 0 { return 0.0 }

    let raw = x as f64 / y as f64;
    round::half_up(raw * 100.0, 0) / 100.0
}