use figment::{Figment, Profile};
use figment::providers::{Format, Serialized, Toml};
use rocket::{Build, Config, Rocket};

pub mod controller;
pub use self::controller::*;

pub fn init() -> Rocket<Build> {
    info!("starting up");

    let figment = Figment::from(rocket::Config::default())
        .merge(Serialized::defaults(Config::default()))
        .merge(Toml::file("Rocket.toml").nested())
        .select(Profile::from_env_or("EVAL_PROFILE", "default"));

    rocket::custom(figment)
        .mount("/api/v1", routes![check_health])

}