use crate::repository::AssessmentRepository;

#[get("/health-check")]
pub(in crate::web) fn check_health() -> &'static str { "OK" }