use crate::model::{AssessmentEntity, EmployeeEntity, Id, Period};
use crate::repository::{AssessmentRepository, CRUDRepository, DB};

pub struct AssessmentRepositoryImpl {}

#[rocket::async_trait]
impl CRUDRepository<AssessmentEntity> for AssessmentRepositoryImpl {
    async fn find_by_id(db: DB, id: Id<AssessmentEntity>) -> Option<AssessmentEntity> {
        todo!()
    }

    async fn find_all(db: DB) -> Vec<AssessmentEntity> {
        todo!()
    }

    async fn save(db: DB, entity: AssessmentEntity) -> AssessmentEntity {
        todo!()
    }

    async fn update(db: DB, entity: AssessmentEntity) -> AssessmentEntity {
        todo!()
    }

    async fn delete(db: DB, id: Id<AssessmentEntity>) {
        todo!()
    }
}

#[rocket::async_trait]
impl AssessmentRepository for AssessmentRepositoryImpl {
    async fn find_by_emp_id_and_period(db: DB, emp_id: Id<EmployeeEntity>, period: Period) -> Option<AssessmentEntity> {
        todo!()
    }

    async fn find_by_period_and_emp_ids(db: DB, period: Period, emp_ids: Vec<Id<EmployeeEntity>>) -> Vec<AssessmentEntity> {
        todo!()
    }

    async fn find_by_period(db: DB, period: Period) -> Vec<AssessmentEntity> {
        todo!()
    }
}