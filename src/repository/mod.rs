use rocket::{Build, Rocket};
use rocket::fairing::{AdHoc, self};
use rocket_db_pools::{Connection, Database, sqlx};
use crate::repository::persistence::assessment::AssessmentRepositoryImpl;

mod repository;
mod persistence;

pub use self::repository::*;

type DB = Connection<Db>;

#[derive(Database)]
#[database("eval360")]
pub struct Db(sqlx::MySqlPool);

async fn run_migrations(rocket: Rocket<Build>) -> fairing::Result {
    match Db::fetch(&rocket) {
        Some(db) => match sqlx::migrate!("db/migration").run(&**db).await {
            Ok(_) => Ok(rocket),
            Err(e) => {
                error!("Failed to initialize SQLx_mysql database: {}", e);
                Err(rocket)
            }
        }
        None => Err(rocket)
    }
}

pub fn init_db() -> AdHoc {
    let assessment = AssessmentRepositoryImpl{};

    AdHoc::on_ignite("SQLx Stage", |rocket| async {
        rocket.attach(Db::init())
              .attach(AdHoc::try_on_ignite("SQLx Migrations", run_migrations))
              .manage(assessment)
    })
}


