use crate::model::*;
use crate::repository::{DB};

#[rocket::async_trait]
pub trait CRUDRepository<T> {
    async fn find_by_id(mut db: DB, id: Id<T>) -> Option<T>;
    async fn find_all(mut db: DB) -> Vec<T>;

    async fn save(mut db: DB, entity: T) -> T;
    async fn update(mut db: DB, entity: T) -> T;
    async fn delete(mut db: DB, id: Id<T>);
}

#[rocket::async_trait]
pub trait AssessmentRepository: CRUDRepository<AssessmentEntity> {
    async fn find_by_emp_id_and_period(mut db: DB, emp_id: Id<EmployeeEntity>, period: Period) -> Option<AssessmentEntity>;
    async fn find_by_period_and_emp_ids(mut db: DB, period: Period, emp_ids: Vec<Id<EmployeeEntity>>) -> Vec<AssessmentEntity>;
    async fn find_by_period(mut db: DB, period: Period) -> Vec<AssessmentEntity>;
}

#[rocket::async_trait]
pub trait ChangelogRepository: CRUDRepository<ChangelogEntity> {
    async fn find_by_period(mut db: DB, period: Period) -> Vec<ChangelogEntity>;
}

#[rocket::async_trait]
pub trait EFormRepository: CRUDRepository<EFormEntity> {
    async fn find_by_rater_id_and_period(mut db: DB, rater_id: Id<EmployeeEntity>, period: Period) -> Vec<EFormEntity>;
    async fn find_recipient_id_by_rater_id_and_period(mut db: DB, rater_id: Id<EmployeeEntity>, period: Period) -> Vec<Id<EmployeeEntity>>;
    async fn find_by_rater_id_and_recipient_id_and_period(
        mut db: DB,
        rater_id: Id<EmployeeEntity>,
        recipient_id: Id<EmployeeEntity>,
        period: Period
    ) -> Option<EFormEntity>;
    async fn find_by_period(mut db: DB, period: Period) -> Vec<EFormEntity>;
    async fn find_by_recipient_id_and_period(mut db: DB, recipient_id: Id<EmployeeEntity>, period: Period) -> Vec<EFormEntity>;
}

#[rocket::async_trait]
pub trait EmployeeRepository: CRUDRepository<EmployeeEntity> {
    async fn find_by_email(mut db: DB, email: String) -> Option<EmployeeEntity>;
    async fn find_by_ids(mut db: DB, ids: Vec<Id<EmployeeEntity>>) -> Vec<EmployeeEntity>;
    async fn find_by_statuses(mut db: DB, lst: Vec<Status>) -> Vec<EmployeeEntity>;
    async fn find_by_role_and_department_and_status(mut db: DB, role: Role, department: Department, status: Status) -> Option<EmployeeEntity>;
    async fn find_live_emp_by_id(mut db: DB, id: Id<EmployeeEntity>) -> Option<EmployeeEntity>;
}

#[rocket::async_trait]
pub trait MFormRepository: CRUDRepository<MFormEntity> {
    async fn find_by_period(mut db: DB, period: Period) -> Vec<MFormEntity>;
    async fn find_by_manager_id_and_period(mut db: DB, mng_id: Id<EmployeeEntity>, period: Period) -> Vec<MFormEntity>;
    async fn find_member_id_by_period(mut db: DB, period: Period) -> Vec<Id<EmployeeEntity>>;
    async fn find_by_member_id_and_period(mut db: DB, mem_id: Id<EmployeeEntity>, period: Period) -> Option<MFormEntity>;
    async fn update_evaluation(mut db: DB, eid: Id<EmployeeEntity>, evaluation: Evaluation) -> MFormEntity;
}

pub trait SettingRepository: CRUDRepository<SettingEntity> {}