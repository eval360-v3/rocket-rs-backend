use std::cmp::Ordering;
use std::fmt::{self, Result as FmtResult};
use std::fmt::Formatter;
use std::marker::PhantomData;

pub struct Version<T>(usize, PhantomData<T>);

impl<T> fmt::Debug for Version<T> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        self.0.fmt(f)
    }
}

impl<T> fmt::Display for Version<T> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        self.0.fmt(f)
    }
}

impl<T> Clone for Version<T> {
    fn clone(&self) -> Self {
        Version(self.0, PhantomData)
    }
}

impl<T> Copy for Version<T> {}

impl<T> Default for Version<T> {
    fn default() -> Self {
        Version(0, PhantomData)
    }
}

impl<T> PartialEq for Version<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<T> Eq for Version<T> {}

impl<T> PartialOrd for Version<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<T> Ord for Version<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T> Version<T> {
    pub fn new() -> Self {
        Version(0, PhantomData)
    }
}
