use std::cmp::Ordering;
use std::fmt::{self, Result as FmtResult};
use std::fmt::Formatter;
use std::marker::PhantomData;

pub struct Id<T>(usize, PhantomData<T>);

impl<T> fmt::Debug for Id<T> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        fmt::Debug::fmt(&self.0, f)
    }
}

impl<T> fmt::Display for Id<T> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        fmt::Display::fmt(&self.0, f)
    }
}

impl<T> Clone for Id<T> {
    fn clone(&self) -> Self {
        Id(self.0, PhantomData)
    }
}

impl<T> Copy for Id<T> {}

impl<T> PartialEq for Id<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<T> Eq for Id<T> {}

impl<T> PartialOrd for Id<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<T> Ord for Id<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}
