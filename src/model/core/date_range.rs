use crate::utility::get_date_vn_now;
use rocket::time::{Date};

#[derive(Debug)]
pub struct DateRange {
    pub start_date: Date,
    pub end_date: Date
}

impl DateRange {
    pub fn contains(&self, target_date: Date) -> bool {
        self.start_date <= target_date && target_date <= self.end_date
    }

    pub fn refresh_to_year_now(self) -> Self {
        let current_date = get_date_vn_now();
        let new_start_date = self.start_date.replace_year(current_date.year()).unwrap();
        let new_end_date = self.end_date.replace_year(current_date.year()).unwrap();

        DateRange {
            start_date: new_start_date,
            end_date: new_end_date
        }
    }
}