use strum_macros::{Display, EnumString, IntoStaticStr};

mod employee;
mod setting;
mod evaluation;
mod assessment;
mod changelog;
mod email_content;
mod internal_event;

pub use self::employee::*;
pub use self::setting::*;
pub use self::changelog::*;
pub use self::evaluation::*;
pub use self::assessment::*;
pub use self::email_content::*;
pub use self::internal_event::*;

#[derive(Debug)]
#[derive(Display, EnumString, IntoStaticStr)]
pub enum Evaluation {
    S, A, B, C, D, E
}
