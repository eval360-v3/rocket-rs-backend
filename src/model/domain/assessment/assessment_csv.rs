use crate::model::{AssessmentEntity, EmployeeEntity};

pub struct AssessmentCsv {
    pub member_id: String,
    pub member_name: String,
    pub member_role: String,
    pub manager_name: String,
    pub member_department: String,
    pub member_rank: String,
    pub avg_c1: String,
    pub avg_c2: String,
    pub avg_c3: String,
    pub avg_c4: String,
    pub avg_c5: String,
    pub avg_c6: String,
    pub avg: String,
    pub similar_rank: String,
    pub draft_hr: String,
    pub draft_mng: String,
    pub final_hr: String,
    pub final_mng: String
}

impl AssessmentCsv {

    pub const HEADERS: [&'static str; 18] = [
        "﻿Id", "Name", "Role", "MIC", "Department", "Rank", "Criteria_1",
        "Criteria_2", "Criteria_3", "Criteria_4", "Criteria_5", "Criteria_6", "Average", "Similar Rank",
        "Draft_Hr", "Draft_Mng", "Final_Hr", "Final_Mng"
    ];

    pub fn new(assessment: AssessmentEntity, member: EmployeeEntity, manager_name: String) -> Self {
        let member_role: &'static str = member.role.into();
        let member_department: &'static str = member.department.into();
        let member_rank: &'static str = member.rank.into();

        AssessmentCsv {
            member_id: member.id.to_string(),
            member_name: member.name,
            member_role: member_role.to_string(),
            manager_name,
            member_department: member_department.to_string(),
            member_rank: member_rank.to_string(),
            avg_c1: assessment.me_point.cal_avg_c1().to_string(),
            avg_c2: assessment.me_point.cal_avg_c2().to_string(),
            avg_c3: assessment.me_point.cal_avg_c3().to_string(),
            avg_c4: assessment.me_point.cal_avg_c4().to_string(),
            avg_c5: assessment.me_point.cal_avg_c5().to_string(),
            avg_c6: assessment.me_point.cal_avg_c6().to_string(),
            avg: assessment.me_point.cal_avg().to_string(),
            similar_rank: assessment.similar_rank_point.cal_avg().to_string(),
            draft_hr: assessment.draft_hr_result.map(|v| {
                let tmp: & str = v.into();
                tmp.to_string()
            }).unwrap_or(String::from("")),
            draft_mng: assessment.draft_mng_result.map(|v| {
                let tmp: & str = v.into();
                tmp.to_string()
            }).unwrap_or(String::from("")),
            final_hr: assessment.final_hr_result.map(|v| {
                let tmp: & str = v.into();
                tmp.to_string()
            }).unwrap_or(String::from("")),
            final_mng: assessment.final_mng_result.map(|v| {
                let tmp: &str = v.into();
                tmp.to_string()
            }).unwrap_or(String::from("")),
        }
    }

    pub fn get_row(&self) -> Vec<String> {
        let mut lst = Vec::new();
        lst.push(self.member_id.clone());
        lst.push(self.member_name.clone());
        lst.push(self.member_role.clone());
        lst.push(self.manager_name.clone());
        lst.push(self.member_department.clone());
        lst.push(self.member_rank.clone());
        lst.push(self.avg_c1.clone());
        lst.push(self.avg_c2.clone());
        lst.push(self.avg_c3.clone());
        lst.push(self.avg_c4.clone());
        lst.push(self.avg_c5.clone());
        lst.push(self.avg_c6.clone());
        lst.push(self.avg.clone());
        lst.push(self.similar_rank.clone());
        lst.push(self.draft_hr.clone());
        lst.push(self.draft_mng.clone());
        lst.push(self.final_hr.clone());
        lst.push(self.final_mng.clone());

        lst
    }
}