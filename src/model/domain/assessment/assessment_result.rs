use crate::model::{AssessmentEntity, EmployeeEntity, Period};

pub struct AssessmentResult<T> {
    pub member: EmployeeEntity,
    pub assessment: AssessmentEntity,
    pub data: T,
    pub period: Period
}

impl <T> AssessmentResult<T> {
    pub fn new(member: EmployeeEntity, assessment: AssessmentEntity, data: T) -> Self<> {
        AssessmentResult {
            member,
            assessment,
            data,
            period: Period::evaluation()
        }
    }
}