use rocket::time::OffsetDateTime;
use crate::model::{EFormGroup, EmployeeEntity, Evaluation, Id, Period, Version};
use crate::utility::div;

pub struct AssessmentEntity {
    pub id: Id<AssessmentEntity>,
    pub employee_id: Id<EmployeeEntity>,
    pub draft_hr_result: Option<Evaluation>,
    pub draft_mng_result: Option<Evaluation>,
    pub final_hr_result: Option<Evaluation>,
    pub final_mng_result: Option<Evaluation>,
    pub me_point: HrSummaryPoint,           // all valid points of me
    pub similar_rank_point: HrSummaryPoint, // all valid points by same rank
    pub superior_point: HrSummaryPoint,     // all valid points from superior
    pub self_point: HrSummaryPoint,         // point from me to me
    pub peer_point: HrSummaryPoint,         // all valid points from peer
    pub all_hr_point: HrSummaryPoint,       // all valid points of company
    pub hr_last_updater_id: Option<Id<EmployeeEntity>>,
    pub mng_last_updater_id: Option<Id<EmployeeEntity>>,
    pub is_sent_email: bool,
    pub period: Period,
    pub version: Version<AssessmentEntity>,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime
}

pub struct HrSummaryPoint {
    pub total_c1point: u32,
    pub total_c1: u32,
    pub total_c2point: u32,
    pub total_c2: u32,
    pub total_c3point: u32,
    pub total_c3: u32,
    pub total_c4point: u32,
    pub total_c4: u32,
    pub total_c5point: u32,
    pub total_c5: u32,
    pub total_c6point: u32,
    pub total_c6: u32,
}

impl HrSummaryPoint {
    pub fn new(group: EFormGroup) -> Self {
        HrSummaryPoint {
            total_c1point: group.get_total_point(1),
            total_c1: group.get_amount(1u8),
            total_c2point: group.get_total_point(2),
            total_c2: group.get_amount(2u8),
            total_c3point: group.get_total_point(3),
            total_c3: group.get_amount(3u8),
            total_c4point: group.get_total_point(4),
            total_c4: group.get_amount(4u8),
            total_c5point: group.get_total_point(5),
            total_c5: group.get_amount(5u8),
            total_c6point: group.get_total_point(6),
            total_c6: group.get_amount(6u8),
        }
    }

    pub fn default() -> Self {
        HrSummaryPoint {
            total_c1point: 0, total_c1: 0,
            total_c2point: 0, total_c2: 0,
            total_c3point: 0, total_c3: 0,
            total_c4point: 0, total_c4: 0,
            total_c5point: 0, total_c5: 0,
            total_c6point: 0, total_c6: 0
        }
    }

    pub fn cal_avg_c1(&self) -> f64 { div(self.total_c1point, self.total_c1) }
    pub fn cal_avg_c2(&self) -> f64 { div(self.total_c2point, self.total_c2) }
    pub fn cal_avg_c3(&self) -> f64 { div(self.total_c3point, self.total_c3) }
    pub fn cal_avg_c4(&self) -> f64 { div(self.total_c4point, self.total_c4) }
    pub fn cal_avg_c5(&self) -> f64 { div(self.total_c5point, self.total_c5) }
    pub fn cal_avg_c6(&self) -> f64 { div(self.total_c6point, self.total_c6) }

    pub fn cal_avg(&self) -> f64 {
        let criteria_amount = self.total_c1 + self.total_c2 + self.total_c3 + self.total_c4 + self.total_c5 + self.total_c6;
        let total_point = self.total_c1point + self.total_c2point +  self.total_c3point + self.total_c4point + self.total_c5point + self.total_c6point;

        div(total_point, criteria_amount)
    }
}