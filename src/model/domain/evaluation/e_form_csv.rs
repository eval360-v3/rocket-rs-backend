use crate::model::domain::evaluation::EFormEntity;
use crate::utility::to_date_time_str;

pub struct EFormCsv {
    pub id: String,
    pub created_at: String,
    pub rater_email: String,
    pub recipient_name: String,
    pub c1point: String,
    pub c1comment: String,
    pub c2point: String,
    pub c2comment: String,
    pub c3point: String,
    pub c3comment: String,
    pub c4point: String,
    pub c4comment: String,
    pub c5point: String,
    pub c5comment: String,
    pub c6point: String,
    pub c6comment: String,
    pub contributes: String,
    pub thanks: String,
    pub challenges: String
}

impl EFormCsv {
    pub const HEADERS:[&'static str; 19] = [
        "﻿Id", "Timestamp", "Email Address", "Anh/chị đang đánh giá thành viên nào?",
        "1. Khi tiến hành công việc, luôn thực hiện một cách có trách nhiệm", "Cơ sở đánh giá (1)",
        "2. Luôn hành động vì lợi ích chung vượt ra ngoài phạm vi công việc của bản thân ", "Cơ sở đánh giá (2)",
        "3. Có đầy đủ khả năng tiến hành nhuần nhuyễn các công việc trong phạm vi trách nhiệm", "Cơ sở đánh giá (3)",
        "4. Khi tiến hành công việc, luôn giao tiếp hiệu quả với các bên liên quan", "Cơ sở đánh giá (4)",
        "5. Luôn hành động theo quy tắc ứng xử chung ", "Cơ sở đánh giá (5)",
        "6. Đánh giá mức độ cống hiến trong kỳ vừa qua", "Cơ sở đánh giá (6)",
        "Những đóng góp của anh/chị ấy cho công ty", "Những thử thách để anh/chị ấy có thể phát triển",
        "Lời cảm ơn"
    ];

    pub fn new(recipient_name: String, rater_email: String, form: EFormEntity) -> Self {
        EFormCsv {
            id: form.id.to_string(),
            created_at: to_date_time_str(form.created_at),
            rater_email,
            recipient_name,
            c1point: form.criteria.get_point_str_by_index(1),
            c1comment: form.criteria.get_comment_str_by_index(1),
            c2point: form.criteria.get_point_str_by_index(2),
            c2comment: form.criteria.get_comment_str_by_index(2),
            c3point: form.criteria.get_point_str_by_index(3),
            c3comment: form.criteria.get_comment_str_by_index(3),
            c4point: form.criteria.get_point_str_by_index(4),
            c4comment: form.criteria.get_comment_str_by_index(4),
            c5point: form.criteria.get_point_str_by_index(5),
            c5comment: form.criteria.get_comment_str_by_index(5),
            c6point: form.criteria.get_point_str_by_index(6),
            c6comment: form.criteria.get_comment_str_by_index(6),
            contributes: form.contributes.unwrap_or("".to_string()),
            thanks: form.thanks.unwrap_or("".to_string()),
            challenges: form.challenges.unwrap_or("".to_string())
        }
    }
}