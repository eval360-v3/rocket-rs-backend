use crate::model::EFormEntity;

pub struct EFormGroup {
    pub forms: Vec<EFormEntity>,
}

impl EFormGroup {
    pub fn default() -> Self { EFormGroup { forms: vec![] } }

    pub fn merge(&self, other: Self) -> Self {
        EFormGroup { forms: [&other.forms[..], &self.forms[..]].concat() }
    }

    pub fn filter<P>(&self, predicate: P) -> Self where P: Fn(&EFormEntity) -> bool {
        let new_forms = self.forms.clone().into_iter()
                            .filter(|v| predicate(v))
                            .collect::<Vec<EFormEntity>>();

        EFormGroup { forms: new_forms }
    }

    pub fn get_total_point(&self, criteria_idx: u8) -> u32 {
        self.forms.iter()
            .filter_map(|item| item.criteria.get_point_by_index(criteria_idx))
            .map(|v| v as u32)
            .sum()
    }

    pub fn get_amount(&self, criteria_idx: u8) -> u32 {
        self.forms.iter()
            .filter_map(|item| item.criteria.get_point_by_index(criteria_idx))
            .count() as u32
    }

    pub fn get_criteria_comments(&self) -> Vec<String> {
        let c1comment = self.merge_comments(|f| f.criteria.c1comment.clone());
        let c2comment = self.merge_comments(|f| f.criteria.c2comment.clone());
        let c3comment = self.merge_comments(|f| f.criteria.c3comment.clone());
        let c4comment = self.merge_comments(|f| f.criteria.c4comment.clone());
        let c5comment = self.merge_comments(|f| f.criteria.c5comment.clone());
        let c6comment = self.merge_comments(|f| f.criteria.c6comment.clone());

        vec![c1comment, c2comment, c3comment, c4comment, c5comment, c6comment]
    }

    pub fn get_extra_comments(&self) -> Vec<String> {
        let contributes = self.merge_comments(|f| f.contributes.clone());
        let thanks = self.merge_comments(|f| f.thanks.clone());
        let challenges = self.merge_comments(|f| f.challenges.clone());

        vec![contributes, thanks, challenges]
    }

    fn merge_comments<P>(&self, transfer: P) -> String where P: Fn(&EFormEntity) -> Option<String> {
        self.forms.iter()
            .filter_map(|v| transfer(v))
            .map(|v| {
                let bytes = base64::decode(v).unwrap();
                String::from_utf8(bytes).unwrap()
            })
            .collect::<Vec<String>>()
            .join("\n")
    }
}