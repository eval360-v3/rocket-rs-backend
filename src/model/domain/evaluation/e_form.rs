use rocket::time::{OffsetDateTime};
use crate::model::{EmployeeEntity, Id, Period};

#[derive(Clone)]
pub struct EFormEntity {
    pub id: Id<EFormEntity>,
    pub recipient_id: Id<EmployeeEntity>,
    pub rater_id: Id<EmployeeEntity>,
    pub contributes: Option<String>,
    pub thanks: Option<String>,
    pub challenges: Option<String>,
    pub criteria: ECriteria,
    pub period: Period,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime
}

impl EFormEntity {
    pub fn can_be_calculated(&self) -> bool { self.recipient_id != self.rater_id }
}

#[derive(Clone)]
pub struct ECriteria {
    pub c1point: Option<u8>,
    pub c1comment: Option<String>,
    pub c2point: Option<u8>,
    pub c2comment: Option<String>,
    pub c3point: Option<u8>,
    pub c3comment: Option<String>,
    pub c4point: Option<u8>,
    pub c4comment: Option<String>,
    pub c5point: Option<u8>,
    pub c5comment: Option<String>,
    pub c6point: Option<u8>,
    pub c6comment: Option<String>
}

impl ECriteria {
    pub const UNOBSERVED: &'static str = "Unobserved";

    pub fn get_point_by_index(&self, idx: u8) -> Option<u8> {
        match idx {
            1 => self.c1point,
            2 => self.c2point,
            3 => self.c3point,
            4 => self.c4point,
            5 => self.c5point,
            6 => self.c6point,

            _ => None
        }
    }

    pub fn get_point_str_by_index(&self, idx: u8) -> String {
        match idx {
            1 => self.c1point.map(|v| v.to_string()).unwrap_or(ECriteria::UNOBSERVED.to_string()),
            2 => self.c2point.map(|v| v.to_string()).unwrap_or(ECriteria::UNOBSERVED.to_string()),
            3 => self.c3point.map(|v| v.to_string()).unwrap_or(ECriteria::UNOBSERVED.to_string()),
            4 => self.c4point.map(|v| v.to_string()).unwrap_or(ECriteria::UNOBSERVED.to_string()),
            5 => self.c5point.map(|v| v.to_string()).unwrap_or(ECriteria::UNOBSERVED.to_string()),
            6 => self.c6point.map(|v| v.to_string()).unwrap_or(ECriteria::UNOBSERVED.to_string()),

            _ => String::from("")
        }
    }

    pub fn get_comment_str_by_index(&self, idx: u8) -> String {
        match idx {
            1 => self.c1comment.clone().unwrap_or_default(),
            2 => self.c2comment.clone().unwrap_or_default(),
            3 => self.c3comment.clone().unwrap_or_default(),
            4 => self.c4comment.clone().unwrap_or_default(),
            5 => self.c5comment.clone().unwrap_or_default(),
            6 => self.c6comment.clone().unwrap_or_default(),

            _ => String::from("")
        }
    }
}