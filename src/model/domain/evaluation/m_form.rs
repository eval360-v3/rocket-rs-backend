use rocket::time::OffsetDateTime;
use crate::model::core::*;
use crate::model::domain::Evaluation;
use crate::model::{EmployeeEntity, Period};

pub struct MFormEntity {
    pub id: Id<MFormEntity>,
    pub member_id: Id<EmployeeEntity>,
    pub manager_id: Id<EmployeeEntity>,
    pub evaluation: Evaluation,
    pub criteria: Vec<String>,
    pub promotion: Promotion,
    pub period: Period,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime
}

pub struct Promotion {
    pub is_promoted: bool,
    pub comment: Option<String>
}
