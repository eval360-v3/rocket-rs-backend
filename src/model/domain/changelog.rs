use rocket::time::format_description::modifier::Period;
use rocket::time::OffsetDateTime;
use crate::model::{EmployeeEntity, Evaluation, Id};
use strum_macros::{Display, EnumString, IntoStaticStr};

#[derive(Debug)]
pub struct ChangelogEntity {
    pub id: Id<ChangelogEntity>,
    pub changer_id: Id<EmployeeEntity>,
    pub changer_name: String,
    pub affected_id: Id<EmployeeEntity>,
    pub affected_name: String,
    pub manager_id: Id<EmployeeEntity>,
    pub manager_name: String,
    pub before_eval: Option<Evaluation>,
    pub after_eval: Option<Evaluation>,
    pub log_type: LogType,
    pub assessment_type: AssessmentType,
    pub period: Period,
    pub created_at: OffsetDateTime
}

#[derive(Debug, Display, EnumString, IntoStaticStr)]
pub enum LogType { Create, Update, Delete }

#[derive(Debug, Display, EnumString, IntoStaticStr)]
pub enum AssessmentType { FinalHr, FinalMng, DraftHr, DraftMng }
