use std::cmp::Ordering;
use std::ops::{Sub};

use rocket::time::{Date};
use strum_macros::{Display, EnumString, IntoStaticStr};

use crate::model::core::*;
use crate::utility::get_date_vn_now;

#[derive(Debug)]
pub struct EmployeeEntity {
    pub id: Id<EmployeeEntity>,
    pub name: String,
    pub email: String,
    pub manager_id: Id<EmployeeEntity>,
    pub working_month_in_period: f64,
    pub gender: Gender,
    pub department: Department,
    pub rank: Rank,
    pub role: Role,
    pub status: Status,
    pub joining_date: Date,
    pub official_joining_date: Option<Date>,
    pub maternity_info: Option<DateRange>,
}

impl EmployeeEntity {
    pub fn get_employee_time(&self) -> String {
        const YEAR_DAYS: u16 = 365;
        const MONTH_DAYS: u8 = 30;

        let current_date = get_date_vn_now();
        let diff_days = current_date.sub(self.joining_date).whole_days();

        let years: u8 = (diff_days / 365) as u8;
        let months: u8 = ((diff_days - years as i64 * YEAR_DAYS as i64) / MONTH_DAYS as i64) as u8;

        let year_str: String = Some(years).filter(|&y| y > 0)
                                          .map(|y| format!("{} years", y))
                                          .unwrap_or(String::from(""));
        let month_str: String = Some(months).filter(|&y| y > 0)
                                            .map(|y| format!("{} months", y))
                                            .unwrap_or(String::from(""));

        vec![year_str, month_str].iter()
                                 .filter(|&str| !str.is_empty())
                                 .map(|str| str.to_string())
                                 .collect::<Vec<String>>()
                                 .join(" and ")
    }

    pub fn can_be_evaluated(&self) -> bool {
        self.working_month_in_period >= 3.0 && self.official_joining_date.is_some()
    }
}

//==========[ Employee Properties ]===============
#[derive(Debug)]
#[derive(Display, EnumString, IntoStaticStr)]
pub enum Department { Boss, BackOffice, Development, Communication }

#[derive(Debug)]
#[derive(Display, EnumString, IntoStaticStr)]
pub enum Status { Active, Inactive, MaternityLeave }

#[derive(Debug)]
#[derive(Display, EnumString, IntoStaticStr)]
pub enum Gender { Male, Female }

#[derive(Debug, PartialEq, Ord)]
#[derive(Display, EnumString, IntoStaticStr)]
pub enum Rank { M, I, A, L, B, IF }

#[derive(Debug, PartialEq)]
#[derive(Display, EnumString, IntoStaticStr)]
pub enum Role {
    Director,
    GeneralManager,
    SeniorManager,
    Manager,
    OfficialEmployeeAboveThreeMonth,
    OfficialEmployeeBelowThreeMonth,
    BackOffice,
    ProbationaryEmployee,
    Intern,
}


//=============[ Property Impls ]================
impl Role {
    pub const MANAGEMENT_ROLES: [Role; 4] = [Role::Director, Role::GeneralManager, Role::SeniorManager, Role::Manager];

    pub fn is_management(&self) -> bool {
        Role::MANAGEMENT_ROLES.contains(&self)
    }
}

impl Rank {
    pub fn get_weight(&self) -> u8 {
        match self {
            Rank::M => 1,
            Rank::I => 2,
            Rank::A => 3,
            Rank::L => 4,
            Rank::B => 5,
            Rank::IF => 10
        }
    }
}

impl Eq for Rank {}

impl PartialOrd<Self> for Rank {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.get_weight().partial_cmp(&other.get_weight())
    }
}

