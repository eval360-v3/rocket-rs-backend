use std::collections::HashMap;
use std::fmt;
use std::fmt::Formatter;
use rocket::time::{Date, Month};
use crate::model::{DateRange, Id, Rank};
use crate::utility::{get_date_vn_now, get_number, to_date_str};

pub struct SettingEntity {
    pub id: Id<SettingEntity>,
    pub period: Period,
    pub mem_expires: DateRange,
    pub mng_expires: DateRange,
    pub gm_expires: DateRange,
    pub gd_expires: DateRange,
    pub min_rank_eval: HashMap<Rank, u8>,
}

impl SettingEntity {
    pub fn refresh_to_now(self) -> Self {
        SettingEntity {
            period: self.period.refresh_to_year_now(),
            mem_expires: self.mem_expires.refresh_to_year_now(),
            mng_expires: self.mng_expires.refresh_to_year_now(),
            gm_expires: self.gm_expires.refresh_to_year_now(),
            gd_expires: self.gd_expires.refresh_to_year_now(),
            ..self
        }
    }
}


#[derive(Clone)]
pub struct Period {
    pub start_date: Date,
    pub end_date: Date
}

#[derive(Eq, PartialEq, Hash)]
pub enum PeriodTag { Now, Previous }

impl Period {
    pub fn get_all_working_periods_in_year() -> HashMap<PeriodTag, Self> {
        let now = get_date_vn_now();
        let current_month = now.month();
        let current_year = now.year();

        let mut period_mapper = HashMap::with_capacity(2);

        let is_month_ge_4 = 4 <= get_number(&current_month);
        let is_month_le_9 = get_number(&current_month) <= 9;
        let is_month_ge_10 = 10 <= get_number(&current_month);

        if is_month_ge_4 && is_month_le_9 {
            period_mapper.insert(PeriodTag::Previous, Period {
                start_date: Date::from_calendar_date(current_year - 1, Month::October, 1).unwrap(),
                end_date: Date::from_calendar_date(current_year, Month::March, 31).unwrap()
            });
            period_mapper.insert(PeriodTag::Now, Period {
                start_date: Date::from_calendar_date(current_year, Month::April, 1).unwrap(),
                end_date: Date::from_calendar_date(current_year, Month::September, 30).unwrap()
            });
        } else if is_month_ge_10 {
            period_mapper.insert(PeriodTag::Previous, Period {
                start_date: Date::from_calendar_date(current_year, Month::April, 1).unwrap(),
                end_date: Date::from_calendar_date(current_year, Month::September, 30).unwrap()
            });
            period_mapper.insert(PeriodTag::Now, Period {
                start_date: Date::from_calendar_date(current_year, Month::October, 1).unwrap(),
                end_date: Date::from_calendar_date(current_year + 1, Month::March, 31).unwrap()
            });
        } else {
            period_mapper.insert(PeriodTag::Previous, Period {
                start_date: Date::from_calendar_date(current_year - 1, Month::April, 1).unwrap(),
                end_date: Date::from_calendar_date(current_year - 1, Month::September, 30).unwrap()
            });
            period_mapper.insert(PeriodTag::Now, Period {
                start_date: Date::from_calendar_date(current_year - 1, Month::October, 1).unwrap(),
                end_date: Date::from_calendar_date(current_year, Month::March, 31).unwrap()
            });
        }

        period_mapper
    }

    pub fn now() -> Self {
        Period::get_all_working_periods_in_year().get(&PeriodTag::Now).unwrap().clone()
    }

    pub fn evaluation() -> Self {
        Period::get_all_working_periods_in_year().get(&PeriodTag::Previous).unwrap().clone()
    }

    pub fn is_equals(&self, other: &Self) -> bool {
        self.start_date == other.start_date && self.end_date == other.end_date
    }

    pub fn previous_evaluation() -> Self {
        let current_period = Period::now();

        Period {
            start_date: current_period.start_date.replace_year(current_period.start_date.year() - 1).unwrap(),
            end_date: current_period.end_date.replace_year(current_period.end_date.year() - 1).unwrap(),
        }
    }

    pub fn refresh_to_year_now(self) -> Self {
        let current_year = get_date_vn_now().year();

        let mut new_start_date = self.start_date.replace_year(current_year).unwrap();
        let new_end_date = self.end_date.replace_year(current_year).unwrap();

        if new_start_date > new_end_date {
            new_start_date = new_start_date.replace_year(current_year - 1).unwrap();
        }

        Period {
            start_date: new_start_date,
            end_date: new_end_date
        }
    }
}

impl fmt::Display for Period {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&format!("{} ~ {}", to_date_str(self.start_date), to_date_str(self.end_date)), f)
    }
}

