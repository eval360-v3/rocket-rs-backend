# Eval360-v3 by Rocket Rust

## Techniques
- Language: Rust
- Framework: [Rocket v0.5](https://rocket.rs/)
- Build tool: Cargo
- Database: [MySql v8.0](https://dev.mysql.com/downloads/mysql/)

## Setup
1. Install Rust: ```https://doc.rust-lang.org/book/ch01-01-installation.html```

## Build & run on localhost
- Build: `cargo build`
- Run: `cargo run`

Application is started at: `http://localhost:8080`

## Build & run on another environments
1. Dev
- Build: `cargo build -r`. The result is in folder: `/target/release/`
- Run: `EVAL_PROFILE=dev ./rocket-rs-eval-backend`

2. Prod
- Build: `cargo build -r`. The result is in folder: `/target/release/`
- Run: `EVAL_PROFILE=prod ./rocket-rs-eval-backend`