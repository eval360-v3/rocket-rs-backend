-- Create employee_tbl table
create table employee_tbl
(
    id                      int primary key,
    `name`                  varchar(255) not null,
    gender                  varchar(10)  not null,
    email                   varchar(100) not null unique,
    manager_id              int          not null,
    department              varchar(100) not null,
    `rank`                  varchar(10)  not null,
    `role`                  varchar(50)  not null,
    joining_date            date         not null,
    official_joining_date   date null,
    maternity_info          varchar(50) null,
    working_month_in_period double       not null,
    status                  varchar(20)  not null,
    created_at              timestamp    not null default current_timestamp,
    updated_at              timestamp    not null default current_timestamp on update current_timestamp
) engine=InnoDB default CHARSET=utf8mb4 default collate utf8mb4_general_ci;

-- Init first employee
Insert into employee_tbl(id, `name`, gender, email, manager_id, department, `rank`, `role`, joining_date, official_joining_date, maternity_info, working_month_in_period, status)
values (162, 'Nguyễn Thị Vân Trang', 'Female', 'trang_ntv@flinters.vn', 35, 'BackOffice', 'I', 'BackOffice', '2019-08-15', '2019-10-15', null, 6, 'Active'),
       (35, 'Nguyễn Thị Lan Hương', 'Female', 'huong_ntl@flinters.vn', 303, 'BackOffice', 'B', 'GeneralManager', '2013-09-03', '2013-11-03', null, 6, 'Active'),
       (70, 'Nguyễn Thị Minh Hạnh', 'Female', 'hanh_nm@flinters.vn', 35, 'BackOffice', 'B', 'BackOffice', '2016-03-16', '2016-05-16', null, 6, 'Active');

-- Create eval_setting_tbl table
create table eval_setting_tbl
(
    id            int primary key auto_increment,
    `period`      varchar(30) not null,
    mem_expires   varchar(30) not null,
    mng_expires   varchar(30) not null,
    gm_expires    varchar(30) not null,
    gd_expires    varchar(30) not null,
    min_rank_eval varchar(255) null,
    created_at    timestamp    not null default current_timestamp,
    updated_at    timestamp    not null default current_timestamp on update current_timestamp
) engine=InnoDB default CHARSET=utf8mb4 default collate utf8mb4_general_ci;

-- Init data
Insert into eval_setting_tbl(period, mem_expires, mng_expires, gm_expires, gd_expires, min_rank_eval)
values ('2021-04-01~2021-09-30', '2021-10-01~2021-10-20', '2021-10-01~2021-10-20', '2021-10-01~2021-10-20', '2021-10-01~2021-10-20', null),
       ('2020-10-01~2021-03-31', '2021-04-01~2021-04-20', '2021-04-01~2021-04-20', '2021-04-01~2021-04-20', '2021-04-01~2021-04-20', null);

-- Create employee_form_tbl table
create table employee_form_tbl
(
    id           int primary key auto_increment,
    recipient_id int  not null,
    rater_id     int  not null,
    criteria     text not null,
    contributes  text null,
    thanks       text null,
    challenges   text null,
    `period`     varchar(30) not null,
    created_at   timestamp   not null default current_timestamp,
    updated_at   timestamp   not null default current_timestamp on update current_timestamp,
    unique (recipient_id, rater_id, `period`),
    foreign key (rater_id) references employee_tbl (id) on delete cascade,
    foreign key (recipient_id) references employee_tbl (id) on delete cascade
) engine=InnoDB default CHARSET=utf8mb4 default collate utf8mb4_general_ci;

-- Create manager_form_tbl table
create table manager_form_tbl
(
    id         int primary key auto_increment,
    member_id  int         not null,
    manager_id int         not null,
    evaluation varchar(1) null,
    criteria   text        not null,
    promotion  text        not null,
    `period`   varchar(30) not null,
    created_at timestamp   not null default current_timestamp,
    updated_at timestamp   not null default current_timestamp on update current_timestamp,
    unique (member_id, manager_id, `period`),
    foreign key (member_id) references employee_tbl (id) on delete cascade,
    foreign key (manager_id) references employee_tbl (id) on delete cascade
) engine=InnoDB default CHARSET=utf8mb4 default collate utf8mb4_general_ci;

-- Create assessment_tbl table
create table assessment_tbl
(
    id                  int primary key auto_increment,
    employee_id         int         not null,
    draft_hr_result     varchar(1) null,
    draft_mng_result    varchar(1) null,
    final_hr_result     varchar(1) null,
    final_mng_result    varchar(1) null,
    hr_last_updater_id  int null,
    mng_last_updater_id int null,
    me_point            varchar(255) null,
    similar_rank_point  varchar(255) null,
    superior_point      varchar(255) null,
    self_point          varchar(255) null,
    peer_point          varchar(255) null,
    all_hr_point        varchar(255) null,
    is_sent_email       boolean     not null default false,
    `period`            varchar(30) not null,
    `version`           smallint    not null default 0,
    created_at          timestamp   not null default current_timestamp,
    updated_at          timestamp   not null default current_timestamp on update current_timestamp
) engine=InnoDB default CHARSET=utf8mb4 default collate utf8mb4_general_ci;

-- Create evaluation_changelog_tbl
CREATE TABLE evaluation_changelog_tbl
(
    id              int primary key auto_increment,
    changer_id      int          not null,
    changer_name    varchar(255) not null,
    affector_id     int          not null,
    affector_name   varchar(255) not null,
    manager_id      int          not null,
    manager_name    varchar(255) not null,
    before_eval     varchar(5)   null,
    after_eval      varchar(5)   null,
    log_type        varchar(20)  not null,
    assessment_type varchar(20)  not null,
    `period`        varchar(30)  not null,
    created_at      timestamp    not null default current_timestamp
) ENGINE = innodb default CHARSET = utf8mb4 default collate utf8mb4_general_ci;
